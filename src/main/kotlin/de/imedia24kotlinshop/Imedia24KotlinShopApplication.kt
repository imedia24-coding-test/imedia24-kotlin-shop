package de.imedia24kotlinshop

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Imedia24KotlinShopApplication

fun main(args: Array<String>) {
    runApplication<Imedia24KotlinShopApplication>(*args)
}
