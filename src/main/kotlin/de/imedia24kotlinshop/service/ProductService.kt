package de.imedia24kotlinshop.service

import de.imedia24kotlinshop.db.entity.ProductEntity
import de.imedia24kotlinshop.db.repository.ProductRepository
import de.imedia24kotlinshop.domain.product.ProductResponse
import de.imedia24kotlinshop.domain.product.ProductResponse.Companion.toProductResponse
import org.springframework.stereotype.Service
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.math.BigDecimal

@Service
class ProductService(private val productRepository: ProductRepository) {
    fun findProductBySku(sku: String): ProductResponse? {
        return findBySku(sku)?.toProductResponse()
    }

    fun findBySku(sku: String): ProductEntity? {
        return productRepository.findBySku(sku)
    }

    fun saveProduct(product: ProductEntity): ProductResponse? {
        return productRepository.save(product).toProductResponse()
    }

    fun createProduct(product: ProductEntity): ProductResponse? {
        return productRepository.save(product).toProductResponse()
    }

    fun findAllBySku(skus: List<String>): List<ProductResponse> {
        if(skus.isEmpty()) {
            throw IllegalArgumentException("The list of SKUs should not be empty")
        }
         return productRepository.findAllById(skus.distinct()).map {it.toProductResponse()}
    }

    fun updateProduct(sku: String, changes: Map<String, Any>): ProductResponse? {
        val allowedChangeKeys = listOf("name", "description", "price")
//    check to validate inputs
        if(sku.isNullOrEmpty()) {
            throw IllegalArgumentException("sku must not be null or empty")
        }
        if(changes.keys.any {it !in allowedChangeKeys}) {
            throw IllegalArgumentException("Only the following keys are allowed to be updated: $allowedChangeKeys")
        }

        val product = findBySku(sku) ?: throw IllegalArgumentException("Product with sku '$sku' not found")

        var editedProduct = product.copy(
                name = changes["name"] as? String ?: product.name,
                description = changes["description"] as? String ?: product.description,
        )

        if(changes.containsKey("price")) {
            var value = changes["price"]

            if(!value.toString().matches(Regex("\\d+(\\.\\d+)?"))) {
                throw IllegalArgumentException("Value for price must be a number")
            }

            editedProduct = editedProduct.copy(
                    price = BigDecimal(value.toString())
            )
        }

        return try {
            saveProduct(editedProduct)
        } catch(e: Exception) {
            throw Exception("Error updating product: ${e.message}")
        }
    }
}