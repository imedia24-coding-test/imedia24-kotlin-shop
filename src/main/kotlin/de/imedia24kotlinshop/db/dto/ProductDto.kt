package de.imedia24kotlinshop.db.dto

import de.imedia24kotlinshop.db.entity.ProductEntity
import java.math.BigDecimal

data class ProductDto(
        var sku: String,
        var name: String,
        var description: String? = null,
        var price: BigDecimal
) {
    fun toProduct() = ProductEntity(
            sku = sku,
            name = name,
            description = description,
            price = price
    )
}
