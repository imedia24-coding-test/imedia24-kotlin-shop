package de.imedia24kotlinshop.controllers

import de.imedia24kotlinshop.db.dto.ProductDto
import de.imedia24kotlinshop.domain.product.ProductResponse
import de.imedia24kotlinshop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/products")
@CrossOrigin("*")
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/{sku}", produces = ["application/json;charset=utf-8"])
    fun finProductBySku(@PathVariable("sku") sku: String): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if (product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }


    @PostMapping
    fun createProduct(@RequestBody product: ProductDto): ResponseEntity<ProductResponse> {
        logger.info("Request to create product $product")
        return ResponseEntity.status(HttpStatus.CREATED).body(productService.saveProduct(product.toProduct()))
    }

    @GetMapping
    fun getListOfProducts(@RequestParam(name="skus") skus: List<String>): ResponseEntity<out Any> {
        return try {
            logger.info("Request for products: $skus")
            ResponseEntity.ok(productService.findAllBySku(skus))
        } catch (e: Exception) {
            logger.error("Error while getting products", e)
            ResponseEntity(e.message, HttpStatus.BAD_REQUEST)
        }
    }

    @PatchMapping("/{sku}")
    fun updateProduct(@PathVariable("sku") sku: String, @RequestBody updates: Map<String, Any>): ResponseEntity<out Any> {
        logger.info("Request to update product $sku with $updates")
        return try {
            val product = productService.updateProduct(sku, updates)
            ResponseEntity.ok(product)
        } catch (e: Exception) {
            logger.error("Error updating product $sku: ${e.message}")
            ResponseEntity(e.message, HttpStatus.BAD_REQUEST)
        }
    }

}