package de.imedia24kotlinshop.controllers

import de.imedia24kotlinshop.db.entity.ProductEntity
import de.imedia24kotlinshop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24kotlinshop.service.ProductService
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerTest(@Autowired private val mockMvc: MockMvc) {

    @MockBean
    private val productService: ProductService? = null

    @Test
    fun createProduct() {
        val sku = "ZG199A"
        val product = ProductEntity(sku, "Product N1", "Description 1", 990.0.toBigDecimal())
        `when`(productService?.saveProduct(product)).thenReturn(product.toProductResponse())

        val body =  """
                    {  
                    "sku": "ZG199A",
                    "name": "Product N1",
                    "description": "Description 1",
                    "price": 990.0
                    }
                """.trimIndent()

        mockMvc.perform(
                post("/products").contentType(MediaType.APPLICATION_JSON).content(
                        body
                )
        ).andExpect(status().isCreated)

    }

    @Test
    fun getProductBySku() {
        val sku = "ZG199A"
        val product = ProductEntity(sku, "Product N1", "Description 1", 990.0.toBigDecimal())
        `when`(productService?.findProductBySku(sku)).thenReturn(product.toProductResponse())

        mockMvc.perform(get("/products/$sku")).andExpect(status().isOk).andExpect(
                content().json(
                        """
                {
                    "sku": "ZG199A",
                    "name": "Product N1",
                    "description": "Description 1",
                    "price": 990.0
                }
            """.trimIndent()
                )
        )
    }

    @Test
    fun getProductsBySkus() {
        val sku = "ZG199A"
        val product = ProductEntity(sku, "Product N1", "Description 1", 990.0.toBigDecimal())
        `when`(productService?.findAllBySku(listOf(sku))).thenReturn(listOf(product.toProductResponse()))

        mockMvc.perform(get("/products?skus=$sku")).andExpect(status().isOk).andExpect(
                content().json(
                        """
                [
                    {
                        "sku": "ZG199A",
                        "name": "Product N1",
                        "description": "Description 1",
                        "price": 990.0
                    }
                ]
            """.trimIndent()
                )
        )

    }

    @Test
    fun editProduct() {
        val sku = "ZG199A"
        val product = ProductEntity(sku, "Product N1", "Description 1", 990.0.toBigDecimal())
        val editedProduct = product.copy(name = "Edited Product N1", description = "Edited Description 1")
        val updates = mapOf("name" to "Edited Product N1", "description" to "Edited Description 1")

        `when`(productService?.findBySku(sku)).thenReturn(product)
        `when`(productService?.updateProduct(sku, updates)).thenReturn(editedProduct.toProductResponse())

        val body = """
                {
                    "name": "Edited Product N1",
                    "description": "Edited Description 1"
                }
            """.trimIndent()

        mockMvc.perform(
                patch("/products/$sku").contentType(MediaType.APPLICATION_JSON).content(
                        body
                )
        ).andExpect(status().isOk).andExpect(
                content().json(
                        """
                {
                    "sku": "ZG199A",
                    "name": "Edited Product N1",
                    "description": "Edited Description 1",
                    "price": 990.0
                }
            """.trimIndent()
                )
        )

    }
}